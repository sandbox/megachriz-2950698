<?php

namespace Drupal\Tests\feedsdev\Kernel\Feeds\Target;

use Drupal\Core\File\FileSystemInterface;
use Drupal\feeds\Plugin\Type\Processor\ProcessorInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\Node;
use Drupal\Tests\feedsdev\Kernel\FeedsDevKernelTestBase;
use Drupal\user\Entity\Role;

/**
 * @coversDefaultClass \Drupal\feedsdev\Feeds\Target\FileBase64
 * @group feedsdev
 */
class FileBase64Test extends FeedsDevKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'feeds',
    'feedsdev',
    'text',
    'filter',
    'file',
    'image',
    'user',
    'system',
  ];

  /**
   * The feed type.
   *
   * @var \Drupal\feeds\FeedTypeInterface
   */
  protected $feedType;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->setUpFileFields();

    // Set default storage backend and configure the theme system.
    $this->installConfig(['user', 'field', 'system']);

    // Give anonymous users permission to access content, so that we can view
    // and download public files.
    $anonymous_role = Role::load(Role::ANONYMOUS_ID);
    $anonymous_role->grantPermission('access content');
    $anonymous_role->save();

    // Allow image field to import png and jpg files.
    FieldConfig::loadByName('node', 'article', 'field_image')
      ->set('settings', ['file_extensions' => 'png jpg'])
      ->save();

    // Create feed type.
    $this->feedType = $this->createFeedTypeForCsv([
      'guid' => 'guid',
      'title' => 'title',
      'file_name' => 'file_name',
      'image' => 'image',
    ]);
  }

  /**
   * Tests importing base64 images.
   */
  public function testImportBase64Image() {
    $this->feedType->addMapping([
      'target' => 'field_image:base64',
      'map' => [
        'base64' => 'image',
        'filename' => 'file_name',
      ],
    ]);
    $this->feedType->save();

    // Import.
    $feed = $this->createFeed($this->feedType->id(), [
      'source' => $this->resourcesPath() . '/content.csv',
    ]);
    $feed->import();
    $this->assertNodeCount(2);

    // Assert that all files were created.
    $files = [
      'druplicon.png',
      'image_test.jpg',
    ];
    foreach ($files as $file) {
      $file_path = $this->container->get('file_system')->realpath('public://' . date('Y-m') . '/' . $file);
      $this->assertFileExists($file_path);
    }
  }

  /**
   * Tests importing base64 images without mapping to filename.
   */
  public function testImportBase64ImageWithoutMappingToFileName() {
    $this->feedType->addMapping([
      'target' => 'field_image:base64',
      'map' => [
        'base64' => 'image',
      ],
      'settings' => [
        // @todo This is going to be a problem for Drupal 12 compatibility.
        // An Enum can not be used as setting.
        'existing' => FileSystemInterface::EXISTS_RENAME,
      ],
    ]);
    $this->feedType->save();

    // Import.
    $feed = $this->createFeed($this->feedType->id(), [
      'source' => $this->resourcesPath() . '/content.csv',
    ]);
    $feed->import();

    $this->assertNodeCount(2);

    // Assert that all files were created.
    $files = [
      'image.png',
      'image_0.png',
    ];
    foreach ($files as $file) {
      $file_path = $this->container->get('file_system')->realpath('public://' . date('Y-m') . '/' . $file);
      $this->assertFileExists($file_path);
    }
  }

  /**
   * Tests if images are deleted when they are no longer in the source.
   */
  public function testFileDeletion() {
    $this->feedType->addMapping([
      'target' => 'field_image:base64',
      'map' => [
        'base64' => 'image',
      ],
      'settings' => [
        // @todo This is going to be a problem for Drupal 12 compatibility.
        // An Enum can not be used as setting.
        'existing' => FileSystemInterface::EXISTS_RENAME,
      ],
    ]);

    // Set to update existing entities.
    $config = $this->feedType->getProcessor()->getConfiguration();
    $config['update_existing'] = ProcessorInterface::UPDATE_EXISTING;
    $this->feedType->getProcessor()->setConfiguration($config);
    $this->feedType->save();

    // Import.
    $feed = $this->createFeed($this->feedType->id(), [
      'source' => $this->resourcesPath() . '/content.csv',
    ]);
    $feed->import();
    $this->assertNodeCount(2);

    // Assert that all files were created.
    $files = [
      'image.png',
      'image_0.png',
    ];
    foreach ($files as $file) {
      $file_path = $this->container->get('file_system')->realpath('public://' . date('Y-m') . '/' . $file);
      $this->assertFileExists($file_path);
    }

    // Now import a file where the images data is no longer present.
    $feed->setSource($this->resourcesPath() . '/content_no_image.csv');
    $feed->save();
    $feed->import();

    // Assert that on the nodes, the files are no longer referenced.
    $nids = [1, 2];
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $this->assertTrue($node->field_image->isEmpty(), 'The image field is emptied.');
    }

    $this->markTestIncomplete('Files are not removed when importing an empty value.');

    // Assert that the files no longer exist.
    foreach ($files as $file) {
      $file_path = $this->container->get('file_system')->realpath('public://' . date('Y-m') . '/' . $file);
      $this->assertFileNotExists($file_path);
    }
  }

}
