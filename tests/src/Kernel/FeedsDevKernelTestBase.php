<?php

namespace Drupal\Tests\feedsdev\Kernel;

use Drupal\Tests\feeds\Kernel\FeedsKernelTestBase;
use Drupal\Tests\feedsdev\Traits\FeedsDevCommonTrait;

/**
 * Base class for Feeds DEV tests.
 */
abstract class FeedsDevKernelTestBase extends FeedsKernelTestBase {

  use FeedsDevCommonTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'node',
    'feeds',
    'feedsdev',
    'text',
    'filter',
    'options',
  ];

}
