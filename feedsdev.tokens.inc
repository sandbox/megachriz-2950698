<?php

/**
 * @file
 * Builds placeholder replacement tokens.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function feedsdev_token_info() {
  return [
    'tokens' => [
      'feeds_feed' => [
        'imported' => [
          'name' => t('Imported'),
          'type' => 'date',
        ],
        'last-import-start' => [
          'name' => t('Last import start'),
          'description' => t('Timestamp of when the last import started that had finished with success.'),
          'type' => 'date',
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function feedsdev_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $replacements = [];

  if ($type == 'feeds_feed' && !empty($data['feeds_feed'])) {
    /** @var \Drupal\feeds\FeedsInterface $feed */
    $feed = $data['feeds_feed'];

    if ($imported_tokens = $token_service->findWithPrefix($tokens, 'imported')) {
      $time = $feed->getImportedTime();
      if (!$time) {
        $time = 1;
      }
      $replacements += $token_service->generate('date', $imported_tokens, ['date' => $time], $options, $bubbleable_metadata);
    }

    if ($imported_tokens = $token_service->findWithPrefix($tokens, 'last-import-start')) {
      $time = \Drupal::state()->get('feeds_feed.last_import_start.' . $feed->id());
      if (!$time) {
        $time = 1;
      }
      $replacements += $token_service->generate('date', $imported_tokens, ['date' => $time], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
