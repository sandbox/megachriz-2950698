<?php

namespace Drupal\feedsdev\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Page callbacks for debugging.
 */
class Debug extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Database connection.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $db
   *   Database connection.
   */
  public function __construct(Connection $db) {
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Returns debug information.
   */
  public function page() {
    if (!empty($_GET['method'])) {
      $method = 'do' . $_GET['method'];
      if (method_exists($this, $method)) {
        $output = $this->$method();
        if ($output) {
          return $output;
        }
      }
    }

    $content = [];

    // Actions.
    $content['actions'] = [
      '#theme' => 'item_list',
      '#items' => [],
    ];

    $feeds_feed_rows = $this->db->select('feeds_feed')
      ->fields('feeds_feed')
      ->execute()
      ->fetchAll();

    $content['feeds_feed'] = $this->buildTable($feeds_feed_rows, [
      'created' => [$this, 'formatDate'],
      'changed' => [$this, 'formatDate'],
      'imported' => [$this, 'formatDate'],
      'next' => [$this, 'formatDate'],
      'queued' => [$this, 'formatDate'],
    ]) + [
      '#caption' => $this->t('Feed sources'),
    ];

    $queue_rows = $this->db->select('queue')
      ->fields('queue')
      ->condition('name', 'feeds_feed_%', 'LIKE')
      ->execute()
      ->fetchAll();

    $content['queue'] = $this->buildTable($queue_rows, [
      'created' => [$this, 'formatDate'],
      'expire' => [$this, 'formatDate'],
      // 'data' => [$this, 'extract'],
    ]) + [
      '#caption' => $this->t('Queue'),
    ];

    foreach ($feeds_feed_rows as $row) {
      $content['actions']['#items'][$row->fid] = [
        '#title' => $this->t('Clear queued for @title', [
          '@title' => $row->title,
        ]),
        '#type' => 'link',
        '#url' => Url::fromRoute('feedsdev.debug', [], [
          'query' => [
            'method' => 'ClearFeedQueued',
            'fid' => $row->fid,
          ],
        ]),
      ];
    }
    $content['actions']['#items']['clear_queue'] = [
      '#title' => $this->t('Clear queue'),
      '#type' => 'link',
      '#url' => Url::fromRoute('feedsdev.debug', [], [
        'query' => [
          'method' => 'ClearQueue',
        ],
      ]),
    ];

    return $content;
  }

  /**
   * Clears the queue.
   */
  protected function doClearQueue() {
    $this->db->truncate('queue')->execute();
  }

  /**
   * Clears the queue.
   */
  protected function doClearFeedQueued() {
    $this->db->update('feeds_feed')
      ->fields([
        'queued' => 0,
      ])
      ->condition('fid', $_GET['fid'])
      ->execute();
  }

  /**
   * Formats value as a date.
   *
   * @param int $timestamp
   *   The timestamp.
   *
   * @return string|null
   *   A date if a correct timestamp was given, null otherwise.
   */
  protected function formatDate($timestamp) {
    if ($timestamp) {
      return date('d-m-Y H:i:s', $timestamp);
    }
  }

  /**
   * Formats value as a date.
   */
  protected function extract($value) {
    if ($value && function_exists('kint_require')) {
      kint_require();
      $data = @unserialize($value);
      return @\Kint::dump($data);
    }
  }

  /**
   * Builds a table from the given result.
   *
   * @param array $table_data
   *   Tabular data to print.
   * @param array $formats
   *   (optional) Format data info for each column.
   *
   * @return array
   *   Table render array.
   */
  protected function buildTable(array $table_data, array $formats = []) {
    if (empty($table_data)) {
      return [];
    }

    // Put in a table.
    $keys = array_keys(get_object_vars($table_data[0]));
    $headers = array_combine($keys, $keys);
    $rows = [];

    $formats += array_fill_keys($keys, '');

    foreach ($table_data as $index => $row) {
      $row = (array) $row;
      $row += array_fill_keys($keys, NULL);

      foreach ($keys as $column) {
        $value = $row[$column];

        if (is_callable($formats[$column])) {
          $function = $formats[$column];
          $value = $function($value);
          $rows[$index][$column]['#markup'] = $value;
          continue;
        }
        elseif (is_string($value)) {
          if (strlen($value) > 255) {
            $value = substr($value, 0, 255) . '...';
          }
        }
        elseif (is_array($value)) {
          $value = 'Array';
        }

        $rows[$index][$column]['#plain_text'] = $value;
      }
    }

    return [
      '#type' => 'table',
      '#header' => $headers,
    ] + $rows;
  }

}
