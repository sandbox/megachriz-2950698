<?php

namespace Drupal\feedsdev\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\feeds\Event\EventDispatcherTrait;
use Drupal\feeds\Event\FeedsEvents;
use Drupal\feeds\Event\FetchEvent;
use Drupal\feeds\Event\InitEvent;
use Drupal\feeds\Event\ParseEvent;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Result\ParserResultInterface;
use Drupal\feeds\StateInterface;
use Drupal\feedsdev\PreviewFeed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lists the feed items belonging to a feed.
 */
class Preview extends ControllerBase {

  use EventDispatcherTrait;

  /**
   * The database connection to be used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a Preview object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * Lists the feed items belonging to a feed.
   */
  public function previewFeed(FeedInterface $feeds_feed, Request $request) {
    // Load PreviewFeed.
    $feeds_feed = PreviewFeed::load($feeds_feed->id());

    /** @var \Drupal\feeds\FeedTypeInterface $feed_type */
    $feed_type = $feeds_feed->getType();

    // Start a transaction that we rollback later to avoid any database writes.
    $transaction = $this->database->startTransaction();

    // Truncate cache_feeds_download to force http download.
    // This truncation is temporary, because the transaction will be rolled
    // back.
    $this->database->truncate('cache_feeds_download')->execute();

    try {
      // Start 'import'.
      $this->dispatchEvent(FeedsEvents::INIT_IMPORT, new InitEvent($feeds_feed, 'fetch'));

      // Fetch.
      $fetch_event = $this->dispatchEvent(FeedsEvents::FETCH, new FetchEvent($feeds_feed));

      // Parse.
      $feeds_feed->setState(StateInterface::PARSE, NULL);
      // Save states to clear any previous pointers.
      $feeds_feed->saveStates();
      $this->dispatchEvent(FeedsEvents::INIT_IMPORT, new InitEvent($feeds_feed, 'parse'));
      $parse_event = $this->dispatchEvent(FeedsEvents::PARSE, new ParseEvent($feeds_feed, $fetch_event->getFetcherResult()));

      $parser_result = $parse_event->getParserResult();

      // Add in custom sources.
      $mappings = $feed_type->getMappings();
      foreach ($parser_result as $item) {
        foreach ($mappings as $mapping) {
          foreach ($mapping['map'] as $column => $source) {
            if (!strlen($source)) {
              continue;
            }
            if ($plugin = $feed_type->getSourcePlugin($source)) {
              $item->set($source, $plugin->getSourceElement($feeds_feed, $item, $source));
            }
          }
        }
      }

      return $this->buildTable($parser_result);
    }
    catch (EmptyFeedException $e) {
      return [
        '#plain_text' => $this->t('No result because the source appears to be empty.'),
      ];
    }
    finally {
      // Undo all database writes that happened during a preview.
      $transaction->rollBack();
    }
  }

  /**
   * Builds a table from the given result.
   *
   * @param \Drupal\feeds\Result\ParserResultInterface $result
   *   The parser result.
   *
   * @return array
   *   Table render array.
   */
  protected function buildTable(ParserResultInterface $result) {
    if ($result->isEmpty()) {
      return [
        '#plain_text' => $this->t('No result.'),
      ];
    }

    // Put in a table.
    $item = $result->offsetGet(0)->toArray();
    $keys = array_keys($item);
    $headers = array_combine($keys, $keys);
    $rows = [];

    $index = 0;
    foreach ($result as $item) {
      $row = $item->toArray();
      $row += array_fill_keys($keys, NULL);

      foreach ($keys as $column) {
        $value = $row[$column];
        $row_value = [
          '#plain_text' => $value,
        ];

        if (is_string($value) && strlen($value) > 255) {
          $value = substr($value, 0, 255) . '...';
        }

        if (is_scalar($value)) {
          $row_value['#plain_text'] = $value;
        }
        elseif (is_array($value)) {
          foreach ($value as &$subvalue) {
            if (is_string($subvalue)) {
              if (strlen($subvalue) > 255) {
                $subvalue = substr($subvalue, 0, 255) . '...';
              }
            }
          }
          $row_value = [
            '#theme' => 'item_list',
            '#items' => $value,
          ];
        }

        $rows[$index][$column] = $row_value;
      }

      $index++;
    }

    return [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'overflow: scroll;',
      ],
      'table' => [
        '#type' => 'table',
        '#header' => $headers,
      ] + $rows,
    ];
  }

}
