<?php

namespace Drupal\feedsdev;

use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\feeds\Entity\Feed;

/**
 * Overrides the default Feed class to prevent saving data.
 */
class PreviewFeed extends Feed {

  /**
   * Returns the storage class that creates PreviewFeed instances.
   *
   * @return \Drupal\feedsdev\PreviewFeedStorage
   *   The preview feed storage.
   */
  protected static function getStorage() {
    $entity_type_id = \Drupal::service('entity_type.repository')
      ->getEntityTypeFromClass(Feed::class);

    $entity_type_manager = \Drupal::entityTypeManager();
    $definition = $entity_type_manager->getDefinition($entity_type_id);
    return $entity_type_manager->createHandlerInstance(PreviewFeedStorage::class, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function load($id) {
    return static::getStorage()->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadMultiple(array $ids = NULL) {
    return static::getStorage()->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    return static::getStorage()->create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  /**
   * Returns the storage on which temporary values for the feed are saved.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   *   The key/value storage collection for this feed.
   */
  protected function getStateStorage(): KeyValueStoreInterface {
    return \Drupal::keyValue('feeds_feed.preview.' . $this->id());
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return FALSE;
  }

}
