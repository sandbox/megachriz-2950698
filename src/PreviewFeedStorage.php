<?php

namespace Drupal\feedsdev;

use Drupal\feeds\FeedStorage;

/**
 * Overrides FeedStorage to provide a different type of entity class.
 */
class PreviewFeedStorage extends FeedStorage {

  /**
   * {@inheritdoc}
   */
  public function getEntityClass(?string $bundle = NULL): string {
    return PreviewFeed::class;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCacheId($id) {
    return "values:{$this->entityTypeId}:preview:$id";
  }

}
