<?php

namespace Drupal\feedsdev\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\State\StateInterface;
use Drupal\feeds\Event\FeedsEvents;
use Drupal\feeds\Event\ImportFinishedEvent;
use Drupal\feeds\Event\InitEvent;
use Drupal\feeds\StateInterface as FeedsStateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Responds to feeds events.
 */
class FeedsImportSubscriber implements EventSubscriberInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new FeedsImportSubscriber object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime service.
   */
  public function __construct(StateInterface $state, TimeInterface $time) {
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    if (class_exists('Drupal\feeds\Event\FeedsEvents')) {
      $events[FeedsEvents::INIT_IMPORT][] = [
        'onInitImport',
        FeedsEvents::BEFORE,
      ];
      $events[FeedsEvents::IMPORT_FINISHED][] = [
        'onImportFinish',
        FeedsEvents::BEFORE,
      ];
    }
    return $events;
  }

  /**
   * Acts on the start of an import.
   */
  public function onInitImport(InitEvent $event) {
    $stage = $event->getStage();
    if ($stage != 'fetch') {
      return;
    }

    // Get state for fetch stage.
    $state = $event->getFeed()->getState(FeedsStateInterface::FETCH);
    if (!isset($state->import_start_time)) {
      $state->import_start_time = $this->time->getRequestTime();
    }
  }

  /**
   * Acts on finishing an import.
   */
  public function onImportFinish(ImportFinishedEvent $event) {
    /** @var \Drupal\feeds\FeedInterface $feed */
    $feed = $event->getFeed();

    $state = $feed->getState(FeedsStateInterface::FETCH);
    if (isset($state->import_start_time)) {
      // Set the time that the import has started.
      $this->state->set('feeds_feed.last_import_start.' . $feed->id(), $state->import_start_time);
    }

  }

}
