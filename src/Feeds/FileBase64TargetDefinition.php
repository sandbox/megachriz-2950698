<?php

namespace Drupal\feedsdev\Feeds;

use Drupal\feeds\FieldTargetDefinition;

/**
 * Provides a field definition wrapped over a field definition.
 */
class FileBase64TargetDefinition extends FieldTargetDefinition {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return t('@label (base64)', [
      '@label' => parent::getLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyLabel($property) {
    switch ($property) {
      case 'base64':
        return t('Base 64 string');

      case 'filename':
        return t('Filename');

      default:
        return parent::getPropertyLabel($property);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDescription($property) {
    switch ($property) {
      case 'base64':
        return t('A base64-encoded file string');

      case 'filename':
        return t('Filename of the base64-encoded file string');

      default:
        return parent::getPropertyDescription($property);
    }
  }

}
