<?php

namespace Drupal\feedsdev\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feedsdev\Feeds\FileBase64TargetDefinition;

/**
 * Defines a image field mapper where the source is base64 encoded.
 *
 * Requires feeds-real-target.patch.
 *
 * @FeedsTarget(
 *   id = "image_base64",
 *   field_types = {"image"}
 * )
 */
class ImageBase64 extends FileBase64 {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FileBase64TargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('base64')
      ->addProperty('filename')
      ->addProperty('alt')
      ->addProperty('title');
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultFilename($file_data) {
    // Assume to be a png.
    $image_resource = imagecreatefromstring($file_data);
    if ($image_resource != FALSE) {
      return 'image.png';
    }
  }

}
