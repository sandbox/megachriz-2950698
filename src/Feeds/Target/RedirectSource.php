<?php

namespace Drupal\feedsdev\Feeds\Target;

/**
 * Defines an address field mapper.
 *
 * @FeedsTarget(
 *   id = "redirect_source",
 *   field_types = {"redirect_source"}
 * )
 */
class RedirectSource extends GenericFieldTargetBase {}
