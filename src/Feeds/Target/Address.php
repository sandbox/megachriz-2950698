<?php

namespace Drupal\feedsdev\Feeds\Target;

/**
 * Defines an address field mapper.
 *
 * @FeedsTarget(
 *   id = "address",
 *   field_types = {"address"}
 * )
 */
class Address extends GenericFieldTargetBase {}
