<?php

namespace Drupal\feedsdev\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a name field mapper.
 *
 * @FeedsTarget(
 *   id = "name",
 *   field_types = {"name"}
 * )
 */
class Name extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FieldTargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('title')
      ->addProperty('given')
      ->addProperty('middle')
      ->addProperty('family')
      ->addProperty('generational')
      ->addProperty('credentials');
  }

}
