<?php

namespace Drupal\feedsdev\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Provides a generic field target.
 *
 * @FeedsTarget(
 *   id = "generic",
 *   deriver = "Drupal\feedsdev\Plugin\Derivative\GenericFieldTarget",
 * )
 */
class GenericFieldTargetBase extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    $properties = $field_definition->getFieldStorageDefinition()->getPropertyDefinitions();

    $definition = FieldTargetDefinition::createFromFieldDefinition($field_definition);
    foreach ($properties as $property_name => $property_definition) {
      // @todo unlist incompatible properties?
      $definition->addProperty($property_name);
    }

    return $definition;
  }

}
