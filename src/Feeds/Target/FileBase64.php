<?php

namespace Drupal\feedsdev\Feeds\Target;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\Exception\TargetValidationException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\FeedTypeInterface;
use Drupal\feeds\Feeds\Target\File;
use Drupal\feeds\Plugin\Type\Processor\EntityProcessorInterface;
use Drupal\feedsdev\Feeds\FileBase64TargetDefinition;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a file field mapper where the source is base64 encoded.
 *
 * Requires feeds-real-target.patch.
 *
 * @FeedsTarget(
 *   id = "file_base64",
 *   field_types = {"file"}
 * )
 */
class FileBase64 extends File {

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $target = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $target->setFileRepository($container->get('file.repository'));
    return $target;
  }

  /**
   * {@inheritdoc}
   */
  public static function targets(array &$targets, FeedTypeInterface $feed_type, array $definition) {
    $processor = $feed_type->getProcessor();

    if (!$processor instanceof EntityProcessorInterface) {
      return $targets;
    }

    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($processor->entityType(), $processor->bundle());

    foreach ($field_definitions as $id => $field_definition) {
      if (in_array($field_definition->getType(), $definition['field_types'])) {
        if ($target = static::prepareTarget($field_definition)) {
          $target->setPluginId($definition['id']);
          $targets[$id . ':base64'] = $target;
        }
      }
    }
  }

  /**
   * Sets the file repository service.
   *
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   */
  public function setFileRepository(FileRepositoryInterface $file_repository) {
    $this->fileRepository = $file_repository;
  }

  /**
   * Returns the field name for the target.
   *
   * @param string $target
   *   The target name.
   *
   * @return string
   *   The field name.
   */
  protected function getFieldNameForTarget(string $target): string {
    [$field_name] = explode(':', $target);
    return $field_name;
  }

  /**
   * {@inheritdoc}
   */
  public function setTarget(FeedInterface $feed, EntityInterface $entity, $target, array $values) {
    return parent::setTarget($feed, $entity, $this->getFieldNameForTarget($target), $values);
  }

  /**
   * {@inheritdoc}
   */
  public function clearTarget(FeedInterface $feed, EntityInterface $entity, string $target) {
    return parent::clearTarget($feed, $entity, $this->getFieldNameForTarget($target));
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(FeedInterface $feed, EntityInterface $entity, $target) {
    return parent::isEmpty($feed, $entity, $this->getFieldNameForTarget($target));
  }

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FileBase64TargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('base64')
      ->addProperty('filename')
      ->addProperty('description');
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    foreach ($values as $column => $value) {
      switch ($column) {
        case 'base64':
          if ($value) {
            $filename = $values['filename'] ?? NULL;
            if (is_string($filename)) {
              $filename = strtolower($filename);
            }
            $values['target_id'] = $this->getBase64File($value, $filename);
          }
          else {
            // Temporary, but not the way to go.
            throw new EmptyFeedException();
            // $values['target_id'] = NULL;
          }
          unset($values['base64']);
          break;

        case 'filename':
          unset($values[$column]);
          break;

        default:
          $values[$column] = (string) $value;
          break;
      }
    }
  }

  /**
   * Returns default file name if no file name was given.
   *
   * @param string $file_data
   *   The raw file data.
   *
   * @return string|null
   *   A file name, if one could be generated. Null otherwise.
   */
  protected function defaultFilename($file_data) {
    // Assume to be text.
    return 'untitled.txt';
  }

  /**
   * Generates a file for the given base64 string.
   *
   * @param string $base64_string
   *   The base64 string.
   * @param string $filename
   *   (optional) The name of the file.
   *   Defaults to the result of ::defaultFilename().
   *
   * @return int|null
   *   The file id or null if there is no filename.
   */
  protected function getBase64File($base64_string, $filename = NULL): ?int {
    // Decode base64-string.
    $file_data = base64_decode($base64_string);

    if (empty($filename)) {
      $filename = $this->defaultFilename($file_data);
    }

    if ($filename) {
      // Prepare destination directory.
      $destination = $this->token->replace($this->settings['uri_scheme'] . '://' . trim($this->settings['file_directory'], '/'));
      $this->fileSystem->prepareDirectory($destination, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY);
      $filepath = $destination . '/' . $filename;

      switch ($this->configuration['existing']) {
        case FileSystemInterface::EXISTS_ERROR:
          if (file_exists($filepath) && $fid = $this->findEntity('uri', $filepath)) {
            return $fid;
          }
          // @todo just use FileExists::Replace when we drop support for Drupal 10.2.
          $replace = class_exists('\Drupal\Core\File\FileExists') ? FileExists::Replace : FileSystemInterface::EXISTS_REPLACE;
          if ($file = $this->fileRepository->writeData($file_data, $filepath, $replace)) {
            return $file->id();
          }
          break;

        default:
          if ($file = $this->fileRepository->writeData($file_data, $filepath, $this->configuration['existing'])) {
            return $file->id();
          }
      }

      // Something bad happened while trying to save the file to the database.
      // We need to throw an exception so that we don't save an incomplete field
      // value.
      throw new TargetValidationException($this->t('There was an error saving the file: %file', [
        '%file' => $filepath,
      ]));
    }

    return NULL;
  }

}
