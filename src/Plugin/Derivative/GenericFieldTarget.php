<?php

namespace Drupal\feedsdev\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides generic Feeds target plugin definitions for fields.
 *
 * @see \Drupal\feedsdev\Feeds\Target\GenericFieldTargetBase
 */
class GenericFieldTarget extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * Constructs a new GenericFieldTarget.
   *
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   */
  public function __construct(FieldTypePluginManagerInterface $field_type_manager) {
    $this->fieldTypeManager = $field_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    return [];
    foreach ($this->fieldTypeManager->getDefinitions() as $field_type_id => $field_type) {
      $this->derivatives[$field_type_id] = $base_plugin_definition;
      $this->derivatives[$field_type_id]['field_types'] = [$field_type_id];
    }
    return $this->derivatives;
  }

}
