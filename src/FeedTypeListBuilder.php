<?php

namespace Drupal\feedsdev;

use Drupal\Core\Entity\EntityInterface;
use Drupal\feeds\FeedTypeListBuilder as FeedTypeListBuilderBase;

/**
 * Alters listing of feed types.
 */
class FeedTypeListBuilder extends FeedTypeListBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();

    return $this->arraySpliceBeforeKey($header, 'operations', [
      'feeds_log' => $this->t('Logging enabled'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);

    return $this->arraySpliceBeforeKey($row, 'operations', [
      'feeds_log' => $entity->getThirdPartySetting('feeds_log', 'status') === FALSE ? $this->t('No') : $this->t('Yes'),
    ]);
  }

  /**
   * Puts element before a specific element in the array.
   */
  protected function arraySpliceBeforeKey(array $array, string $key, $array_to_insert): array {
    $key_pos = array_search($key, array_keys($array));
    if ($key_pos !== FALSE) {
      $second_array = array_splice($array, $key_pos);
      $array = array_merge($array, $array_to_insert, $second_array);
    }
    return $array;
  }

}
